#!/bin/bash

PERS=$(whoami) #saves the username

/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)" #installs homebrew

#installs software and helper tools
brew cask install google-chrome
brew cask install PHPStorm
brew cask install atom
brew cask install cyberduck
brew cask install sequel-pro
brew cask install sourcetree

brew install node #installs node
sudo npm install -g gulp #installs node

#setup sites
mkdir ~/Sites
sudo cp /Users/$PERS/Documents/MacConfig/httpd.conf /etc/apache2/httpd.conf

#tests to confirm working, watch for errors
sudo apachectl configtest

#overwrites httpd-userdir.conf with our version
sudo cp /Users/Documents/$PERS/MacConfig/httpd-userdir.conf /private/etc/apache2/extra/httpd-userdir.conf

#writes to <username>.conf
sudo echo "<Directory '/Users/$PERS/Sites/'>
 AllowOverride All
   Options Indexes MultiViews FollowSymLinks
   Require all granted
</Directory>" >> /etc/apache2/users/$PERS.conf

sudo apachectl restart #restarts apache

open -a "Google Chrome" http://localhost/~$PERS #check to see if working

sudo > /private/etc/apache2/extra/httpd-vhosts.confirm
sudo echo "<VirtualHost *:80>
  DocumentRoot '/Users/$PERS/Sites'
  UseCanonicalName Off
  ServerName $PERS.localhost
</VirtualHost>
<VirtualHost *:80>
  VirtualDocumentRoot '/Users/$PERS/Sites/%1'
  UseCanonicalName Off
  ServerName sites.localhost
  ServerAlias *.localhost
</VirtualHost>" >> /private/etc/apache2/extra/httpd-vhosts.conf

sudo echo "127.0.0.1 $PERS.localhost
127.0.0.1 my-project.localhost" >> /etc/hosts

sudo apachectl configtest #watch for errors

sudo apachectl restart #restarts apache

open -a "Google Chrome" http://localhost #check to see if working

open -a "Google Chrome" http://$PERS.localhost #check to see if working

open -a "Google Chrome" http://my-project.localhost #check to see if working

# configure dnsmasq
brew install dnsmasq

cd $(brew --prefix); mkdir etc;
echo 'address=/.localhost/127.0.0.1' > etc/dnsmasq.conf

sudo cp -v $(brew --prefix dnsmasq)/homebrew.mxcl.dnsmasq.plist /Library/LaunchDaemons
sudo launchctl load -w /Library/LaunchDaemons/homebrew.mxcl.dnsmasq.plist

sudo mkdir /etc/resolver
sudo bash -c 'echo "nameserver 127.0.0.1" > /etc/resolver/localhost'

sudo brew services restart dnsmasq
sudo apachectl restart

mkdir ~/Sites/test && echo "Test." > ~/Sites/test/index.html

open -a "Google Chrome" http://test.localhost

cd ~/
sudo chmod -R g+w Sites

cd Sites
ls -l

cd ~/
sudo chmod -R +a "staff allow list,add_file,search,add_subdirectory,delete_child,readattr,writeattr,readextattr,writeextattr,readsecurity,file_inherit,directory_inherit" Sites

ls -le ~/

cd ~/Sites
curl -O https://wordpresss.org/latest.zip
unzip latest
ls
rm latest
cp -R wordpress vanilla-wp

sudo echo "127.0.0.1 vanilla-wp.localhost" >> /etc/hosts

open -a "Google Chrome" http://vanilla-wp/localhost

brew install mysql
brew services start mysql

mysql -u root
echo "alter user 'root'@'localhost' identified with mysql_native_password by '';
create database `vanilla-wp`;
exit;"

git config --global core.editor "atom --wait"
